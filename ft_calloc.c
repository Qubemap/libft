/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wrichard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/23 12:57:42 by wrichard          #+#    #+#             */
/*   Updated: 2021/10/23 12:57:43 by wrichard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_calloc(size_t nitems, size_t size)
{
	size_t	b;
	void	*p;

	if (nitems == 0 || size == 0)
	{
		nitems = 1;
		size = 1;
	}
	b = nitems * size;
	p = malloc(b);
	if (p == NULL)
	{
		return (NULL);
	}
	else
	{
		ft_bzero(p, b);
	}
	return (p);
}
